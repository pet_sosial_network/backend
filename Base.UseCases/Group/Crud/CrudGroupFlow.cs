﻿using Base.Core;
using Base.Core.Schemas;
using Base.Services;
using Base.Utils; 

namespace Base.UseCases.Group.Crud
{
    public class CrudGroupFlow
    {
        private readonly IUnitOfWork uow;
        public CrudGroupFlow(IUnitOfWork _uow)
        {
            uow = _uow;
        }

        public UseCaseResponse List()
        {
            var groups = uow.Groups.FindAll();
            return new UseCaseResponse(Message.SUCCESS, groups);
        }

        public async Task<UseCaseResponse> Create(GroupSchema group)
        {
            var result = uow.Groups.Create(group);
            return new UseCaseResponse(Message.SUCCESS, result);
        }

        public async Task<UseCaseResponse> Update(GroupSchema group)
        {
            var result = uow.Groups.Update(group);
            return new UseCaseResponse(Message.SUCCESS, result);
        }

        public UseCaseResponse Deletes(int[] ids)
        {
            var result = uow.Users.Deletes(ids);
            return new UseCaseResponse(Message.SUCCESS, result);
        }

    }
}
