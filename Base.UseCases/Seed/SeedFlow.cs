﻿using Base.Business.Rule;
using Base.Core;
using Base.Core.Model;
using Base.Core.Schemas;
using Base.Services;
using Base.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using static Base.Utils.CtrlUtil;

namespace Base.UseCases.Seed
{
    public class SeedFlow
    {
        private readonly IUnitOfWork uow;
        public SeedFlow(IUnitOfWork _uow)
        {
            uow = _uow;
        }

        public async Task<UseCaseResponse> Seed(List<RouterModel> routers)
        {
            bool isSeeded = false;
            var executionStrategy = uow.CreateExecutionStrategy();

            await executionStrategy.Execute(async () =>
            {
                using IDbContextTransaction transaction = uow.BeginTransaction();
                try
                {
                    SeedGroup();
                    SeedUser();
                    List<Perm> perms = await CreatePerms(routers);
                    List<GroupSchema> groups = await CreateGroupPerm(perms);
                    AddUserToGroup(groups);
                    isSeeded = true;
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    isSeeded = false;
                    transaction.Rollback();
                    throw;
                }
            });
            UseCaseResponse response = new UseCaseResponse("success", isSeeded);
            return response;
        }

        private async Task<List<GroupSchema>> CreateGroupPerm(List<Perm> perms)
        {
            List<GroupSchema> groups = uow.Groups.FindAll();
            List<GroupPerm> groupsPerms = new();
            foreach (var perm in perms)
            {
                foreach (var group in groups)
                {
                    if (group.ProfileType.Contains(perm.ProfileTypes) || perm.ProfileTypes == RoleType.PUBLIC_PROFILE)
                    {
                        GroupPerm gp = new GroupPerm();
                        gp.Perm = perm;
                        gp.Group = group;
                        groupsPerms.Add(gp);
                    }
                }
            }
            await uow.GroupsPerms.CreatesAsync(groupsPerms);
            return groups;
        }

        private async Task<List<Perm>> CreatePerms(List<RouterModel> routers)
        {
            List<Perm> perms = new List<Perm>();
            foreach (var router in routers)
            {
                Perm permSchema = new Perm();
                permSchema.Action = router.Method;
                permSchema.Title = router.Method + " " + router.Module;
                permSchema.ProfileTypes = router.ProfileType;
                permSchema.Module = router.Module;
                perms.Add(permSchema);
            }
            perms = await uow.Perms.CreatesAsync(perms);
            return perms;
        }

        private void AddUserToGroup(List<GroupSchema> groups)
        {
            List<UsersGroups> usersGroups = new List<UsersGroups>();
            List<UserSchema> users = uow.Users.FindAll();

            foreach (var user in users)
            {
                foreach (var group in groups)
                {
                    if (user.GroupIds.Contains(group.ProfileType))
                    {
                        UsersGroups ug = new UsersGroups();
                        ug.User = user;
                        ug.Group = group;
                        usersGroups.Add(ug);
                    }
                }
            }
            uow.UsersGroups.Creates(usersGroups);
        }

        private void SeedGroup()
        {
            var currentGroups = uow.Groups.Find(new int[] { 1, 2 });
            if (currentGroups.Count == 0)
            {
                GroupSchema admin = new GroupSchema { Id = 1, Title = "Admin", Description = "", ProfileType = RoleType.ADMIN_PROFILE };
                GroupSchema staff = new GroupSchema { Id = 2, Title = "Staff", Description = "", ProfileType = RoleType.STAFF_PROFILE };
                List<GroupSchema> groups = new List<GroupSchema> { admin, staff };
                uow.Groups.Creates(groups);
            }
        }

        private void SeedUser()
        {
            var currentUsers = uow.Users.Find(new int[] { 1, 2 });
            if (currentUsers.Count == 0)
            {
                string defaultPassword = JwtUtil.MD5Hash(UserRule.DEFAULT_PASSWORD);
                UserSchema userAdmin = new UserSchema { Id = 1, UserName = "admin", Password = defaultPassword, Email = "admin@gmail.com", GroupIds = RoleType.ADMIN_PROFILE };
                UserSchema userStaff = new UserSchema { Id = 2, UserName = "staff", Password = defaultPassword, Email = "staff@gmail.com", GroupIds = RoleType.STAFF_PROFILE };
                List<UserSchema> users = new List<UserSchema> { userAdmin, userStaff };
                uow.Users.Creates(users);
            }

        }
    }
}
