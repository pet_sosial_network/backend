﻿

using Base.Core;
using Base.Core.Schemas;
using Base.Services;
using Base.Utils;
using System.IdentityModel.Tokens.Jwt;

namespace Base.UseCases.Auth
{
    public class AuthFlow
    {
        private readonly IUnitOfWork uow;
        public AuthFlow(IUnitOfWork _uow)
        {
            uow = _uow;
        }

        public UseCaseResponse Login(string username, string password)
        {
            List<UserSchema> users = uow.Users.FindByName(username);

            UserSchema user = users.FirstOrDefault();
            bool isMatched = JwtUtil.Compare(password, user.Password);
            if (!isMatched)
            {
                return new UseCaseResponse(Message.ERROR, new { });
            }
            string accessToken = JwtUtil.GenerateAccessToken(user.Id);
            string refreshToken = JwtUtil.GenerateRefreshToken();
            uow.Users.SetRefreshToken(refreshToken, user.Id);
            uow.Users.UpdateLoginTime(user.Id);
           
            return new UseCaseResponse(Message.SUCCESS, new  { AccessToken = accessToken, RefreshToken = refreshToken, User = user });
        }

        public UseCaseResponse RefreshToken(string accessToken, string refreshToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(accessToken);
            var userCredentialString = jwtToken.Claims.First(x => x.Type == "id").Value;
            int userId = Int32.Parse(userCredentialString);
            UserSchema user = uow.Users.FindOne(userId);
            bool isMatched = user.HashRefreshToken.Equals(refreshToken);
            if (isMatched)
            {
                var newToken = JwtUtil.GenerateAccessToken(userId);
                var newRefreshToken = JwtUtil.GenerateRefreshToken();

                return new UseCaseResponse(Message.SUCCESS, new
                {
                    AccessToken = newToken,
                    RefreshToken = newRefreshToken
                });
            }
            return new UseCaseResponse(Message.ERROR, new { });
        }
    }
}
