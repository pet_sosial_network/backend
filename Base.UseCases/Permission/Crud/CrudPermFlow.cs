﻿using Base.Core.Schemas;
using Base.Core;
using Base.Services;
using Base.Utils; 

namespace Base.UseCases.Permission.Crud
{
    public class CrudPermFlow
    {
        private readonly IUnitOfWork uow;
        public CrudPermFlow(IUnitOfWork _uow)
        {
            uow = _uow;
        }

        public UseCaseResponse List()
        {
            var groups = uow.Perms.FindAll();
            return new UseCaseResponse(Message.SUCCESS, groups);
        }

        public async Task<UseCaseResponse> Create(Perm group)
        {
            var result = uow.Perms.Create(group);
            return new UseCaseResponse(Message.SUCCESS, result);
        }

        public async Task<UseCaseResponse> Update(Perm group)
        {
            var result = uow.Perms.Update(group);
            return new UseCaseResponse(Message.SUCCESS, result);
        }

        public UseCaseResponse Deletes(int[] ids)
        {
            var result = uow.Users.Deletes(ids);
            return new UseCaseResponse(Message.SUCCESS, result);
        }
    }
}
