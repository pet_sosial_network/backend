﻿using Base.Business.Rule;
using Base.Core;
using Base.Core.Schemas;
using Base.Services;
using Base.Utils; 

namespace Base.UseCases.User.Crud
{
    public class CrudUserFlow
    {
        private readonly IUnitOfWork uow;
        public CrudUserFlow(IUnitOfWork _uow)
        {
            uow = _uow;
        }

        public UseCaseResponse GetCurrentUser(string token)
        {
            string userCredentialString = JwtUtil.GetIdByToken(token);
            int id = 0;
            if (!Int32.TryParse(userCredentialString, out id))
            {
                return new UseCaseResponse(Message.ERROR, null);
            }
            UserSchema user = uow.Users.FindOne(id);
            return new UseCaseResponse(Message.SUCCESS, user);
        }

        public async Task<UseCaseResponse> List()
        {
            var users = await uow.Users.FindAllAsync();
            users = users.Where(u => u.Id != UserRule.ADMIN_ID).ToList();
            return new UseCaseResponse(Message.SUCCESS, users);
        }

        public async Task<UseCaseResponse> Create(UserSchema user)
        {
            user.Password = JwtUtil.MD5Hash(UserRule.DEFAULT_PASSWORD);
            var result = uow.Users.Create(user);
            return new UseCaseResponse(Message.SUCCESS, result);
        }

        public async Task<UseCaseResponse> Update(UserSchema user)
        {
            var result = uow.Users.Update(user);
            return new UseCaseResponse(Message.SUCCESS, result);
        }

        public async Task<UseCaseResponse> Deletes(int[] ids)
        {
            var result = uow.Users.Deletes(ids);
            return new UseCaseResponse(Message.SUCCESS, result);
        }

    }
}
