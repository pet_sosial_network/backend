﻿namespace Base.Core
{
    public class UseCaseResponse
    {
        public string Status { get; set; }
        public object Result { get; set; }
        public UseCaseResponse() { }
        public UseCaseResponse(string status, object data)
        {
            Status = status;
            Result = data;
        }
    }


}
