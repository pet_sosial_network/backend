﻿using System.ComponentModel.DataAnnotations;

namespace Base.Core.Presenters.Auth
{
    public class AuthPresenter
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
