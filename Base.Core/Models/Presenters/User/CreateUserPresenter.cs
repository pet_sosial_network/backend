﻿using System.ComponentModel.DataAnnotations;

namespace Base.Core.Models.Presenters.User
{
    public class CreateUserPresenter
    {
        [Required]
        public string Username { get; set; }
        public string? Email { get; set; }
        [Required]
        public string GroupIds { get; set; }
    }
}
