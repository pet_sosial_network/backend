﻿ 

namespace Base.Utils
{ 
    public class ResponsePresenter
    {
        public object Items { get; set; }
        public bool HasNextPage { get; set; }
    }
}
