﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Base.Application.Helpers;
using Base.Application.Routers;
using Base.Core.Exception;
using Base.Core.Model;
using Base.Core.Schemas;
using Base.Services;
using Base.Utils;

namespace Base.Application.Middlewares
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IMemoryCache cacheService;

        public JwtMiddleware(RequestDelegate next, IMemoryCache _cacheService)
        {
            _next = next;
            cacheService = _cacheService;
        }

        public async Task Invoke(HttpContext context, IUnitOfWork uow)
        {
            try
            {
                IEnumerable<RouterModel> routers = PublicRouter.Get(); 
                var publicApi = routers.Select(x=>x.Path).ToList();
                var apiRequest = context.Request.Path.Value.Replace("/api/","");
                if (!publicApi.Contains(apiRequest))
                {
                    var cookieToken = context.Request.Cookies["access_token"];
                    var tokenHeader = context.Request.Headers["Authorization"].FirstOrDefault();
                    if (cookieToken != null || (tokenHeader != null && tokenHeader != "null"))
                    {
                        string token = cookieToken == null ? tokenHeader.Replace("Bearer ", "") : cookieToken;
                        AttachUserToContext(context, uow, token);
                    }
                    else
                    {
                        throw new UnauthorizedException("Unauthorized");
                    }
                }
                await _next(context);

            }
            catch (Exception ex)
            {
                var middlewareHelper = new MiddlewareHelper();
                await middlewareHelper.HandleExceptionAsync(context, ex);
            }
        }

        private void AttachUserToContext(HttpContext context, IUnitOfWork uow, string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(JwtUtil.SECRET_KEY);
            TokenValidationParameters tokenValidationParameters = new()
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            };
            tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            var userCredentialString = jwtToken.Claims.First(x => x.Type == "id").Value;
            int id = Int32.Parse(userCredentialString);
            if (id > 0)
            {
                var header = context.Request.Path.Value;
                var httpMethod = context.Request.Method;
                bool isRequestApi = header.Contains("/api/");
                if (isRequestApi)
                {
                    var apiRequest = header.Replace("/api/", "").Split("/");

                    string module = GetModule(apiRequest);
                    string action = GetAction(apiRequest, httpMethod);
                    ICacheService<Perm> permCacheService = new CacheService<Perm>(cacheService);

                    var perms = permCacheService.Get(CacheKey.PERM_CACHE_KEY + id);
                    if (perms == null || perms.Count == 0)
                    {
                        perms = uow.Perms.GetPerms(id);
                        cacheService.Set(CacheKey.PERM_CACHE_KEY, perms);
                    }
                    Perm perm = perms.Where(x => x.Module.Equals(module) && x.Action.ToLower().Equals(action)).FirstOrDefault();
                    if (perm == null)
                    {
                        throw new ForbiddenException("Forbidden");
                    }
                }
            }
        }


        private string GetModule(string[] apiRequest)
        {
            string module = "";
            if (apiRequest.Length > 1)
            {
                module = apiRequest[1];
            }
            else
            {
                module = apiRequest[0];
            }
            return module;
        }

        private string GetAction(string[] apiRequest, string httpMethod)
        {
            string action = "";
            if (apiRequest.Length > 1)
            {
                var isNumeric = apiRequest.Length == 2 ? int.TryParse(apiRequest[1], out int n) : false;
                action = isNumeric ? httpMethod : apiRequest[0] + "/" + apiRequest[1];
            }
            else
            {
                action = httpMethod;
            }
            return action;
        }
    }

}
