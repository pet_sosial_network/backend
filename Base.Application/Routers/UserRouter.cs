﻿using Base.Application.Controllers;
using Base.Core.Model;
using Base.Core.Models.Presenters.User;
using Base.Services;
using static Base.Utils.CtrlUtil;

namespace Base.Application.Routers
{
    public static class UserRouter
    {
        public static List<RouterModel> Get()
        {
            List<RouterModel> routers = new List<RouterModel>();
            var userGet = new RouterModel()
            {
                Method = "GET",
                Module = "Users",
                Path = "users",
                ProfileType = RoleType.ADMIN_PROFILE,
                Function = async (IUnitOfWork uow) => await UserCtrl.GetAsync(uow)
            };

            var userCreate = new RouterModel()
            {
                Method = "POST",
                Module = "Users",
                Path = "users",
                ProfileType = RoleType.ADMIN_PROFILE,
                Function = async (CreateUserPresenter model, IUnitOfWork uow) => await UserCtrl.CreateAsync(model, uow)
            };

            routers.Add(userGet);
            routers.Add(userCreate);
            return routers;
        }
    }
}
