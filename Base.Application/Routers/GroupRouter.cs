﻿using Base.Application.Controllers;
using Base.Core.Model;
using Base.Services;
using static Base.Utils.CtrlUtil;

namespace Base.Application.Routers
{
    public static class GroupRouter
    {
        public static List<RouterModel> Get()
        {
            List<RouterModel> routers = new List<RouterModel>();
            var loginRouter = new RouterModel()
            {
                Method = "GET",
                Module = "Groups",
                Path = "groups",
                ProfileType = RoleType.ADMIN_PROFILE,
                Function = async (IUnitOfWork uow) =>await GroupCtrl.GetAsync(uow)
            };
            routers.Add(loginRouter);
            return routers;
        }
    }
}
