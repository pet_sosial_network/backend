﻿using Base.Application.Controllers;
using Base.Core.Model;
using Base.Services;
using static Base.Utils.CtrlUtil;

namespace Base.Application.Routers
{
    public static class PermRouter
    {
        public static List<RouterModel> Get()
        {
            List<RouterModel> routers = new List<RouterModel>();
            var loginRouter = new RouterModel()
            {
                Method = "GET",
                Module = "Permissions",
                Path = "permissions",
                ProfileType = RoleType.ADMIN_PROFILE,
                Function = async (IUnitOfWork uow) => await PermCtrl.GetAsync(uow)
            };
            routers.Add(loginRouter);
            return routers;
        }
    }
}
