﻿using Base.Core.Model;

namespace Base.Application.Routers
{
    public static class ZRouter
    {
        public static List<RouterModel> Get()
        {
            List<RouterModel> routers = new List<RouterModel>();
            IEnumerable<RouterModel> publicRouter = PublicRouter.Get(); 
            IEnumerable<RouterModel> userRouter = UserRouter.Get();
            IEnumerable<RouterModel> groupRouters = GroupRouter.Get();
            IEnumerable<RouterModel> permRouters = PermRouter.Get();

            IEnumerable<RouterModel>[] arrs = new IEnumerable<RouterModel>[]
            {
                publicRouter,
                userRouter,
                groupRouters,
                permRouters
            };

            for (int i = 0; i < arrs.Length; i++)
            {
                routers = routers.Union(arrs[i]).ToList();
            }
            return routers;
        }
    }
}
