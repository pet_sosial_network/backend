﻿using Base.Application.Controllers;
using Base.Core.Model;
using Base.Services;
using static Base.Utils.CtrlUtil;

namespace Base.Application.Routers
{
    public static class PublicRouter
    {
        public static List<RouterModel> Get()
        {
            List<RouterModel> routers = new List<RouterModel>();
            var seedRouter = new RouterModel()
            {
                Method = "GET",
                Module = "Seed",
                Path = "seed",
                ProfileType = RoleType.PUBLIC_PROFILE,
                Function = async (IUnitOfWork uow) => await SeedCtrl.SyncAllPerm(uow)
            }; 

            var loginRouter = new RouterModel()
            {
                Method = "POST",
                Module = "Auth",
                Path = "auth/login",
                ProfileType = RoleType.PUBLIC_PROFILE,
                Function = (string username, string password, IUnitOfWork uow) => AuthCtrl.Login(username, password, uow)
            };

            var refreshTokenRouter = new RouterModel()
            {
                Method = "GET",
                Module = "Auth",
                Path = "auth/refresh-token",
                ProfileType = RoleType.PUBLIC_PROFILE,
                Function = (string accessToken, string refreshToken, IUnitOfWork uow) => AuthCtrl.RefreshToken(accessToken, refreshToken, uow)
            };

            routers.Add(seedRouter);
            routers.Add(refreshTokenRouter);
            routers.Add(loginRouter);
            return routers;
        }
    }
}
