using System.Text;
using Microsoft.EntityFrameworkCore;
using Base.Application.Helpers;
using Base.Application.Middlewares;
using Base.Services;
using Base.Utils;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAll", x => x.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod());
});

builder.Services.AddMemoryCache();
byte[] key = Encoding.ASCII.GetBytes(JwtUtil.SECRET_KEY);
string connectionString = builder.Configuration.GetConnectionString("Default");
builder.Services.SwaggerConfig();
builder.Services.JwtConfig(key);
builder.Services.DatabaseConnection(connectionString);
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AutoMapperConfig();
var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "Base Core V3");
    });
}
app.UseAuthentication();
app.UseHttpsRedirection();
app.UseCors("AllowAll");
app.HandleRequest();
app.UseMiddleware<JwtMiddleware>();
app.UseMiddleware<ExceptionMiddleware>();


app.Run();
