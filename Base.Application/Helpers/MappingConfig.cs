﻿using Base.Application.DataRequestMapping;

namespace Base.Application.Helpers
{
    public static class MappingConfig
    {
        public static IServiceCollection AutoMapperConfig(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(CreateUserMapping));
            return services;
        }
    }
}
