﻿using Base.Application.Routers;
using Base.Core.Model;

namespace Base.Application.Helpers
{
    public static class ConfigRouter
    {
        public static WebApplication HandleRequest(this WebApplication app)
        {
            string prefix = "api/";
            List<RouterModel> routers = ZRouter.Get();
            foreach (RouterModel router in routers)
            {
                switch (router.Method)
                {
                    case "GET":
                        app.MapGet(prefix + router.Path, router.Function);
                        break;
                    case "POST":
                        app.MapPost(prefix + router.Path, router.Function);
                        break;
                    case "PUT":
                        app.MapPut(prefix + router.Path, router.Function);
                        break;
                    case "DELETE":
                        app.MapDelete(prefix + router.Path, router.Function);
                        break;
                }
            }
            return app;
        }
    }
}
