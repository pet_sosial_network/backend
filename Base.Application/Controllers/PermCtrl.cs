﻿using Base.Core;
using Base.Services;
using Base.UseCases.User.Crud;

namespace Base.Application.Controllers
{
    public class PermCtrl
    {
        public static async Task<IResult> GetAsync(IUnitOfWork uow)
        {
            CrudUserFlow workflow = new CrudUserFlow(uow);
            UseCaseResponse response = await workflow.List();
            return Results.Ok(response);
        }
    }
}
