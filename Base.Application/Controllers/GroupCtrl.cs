﻿using Base.Core.Schemas;
using Base.Core;
using Base.Services;
using Base.UseCases.User.Crud;
using Base.Core.Models.Presenters.User;

namespace Base.Application.Controllers
{
    public class GroupCtrl
    {
        public static async Task<IResult> GetAsync(IUnitOfWork uow)
        {
            CrudUserFlow workflow = new CrudUserFlow(uow);
            UseCaseResponse response = await workflow.List();
            return Results.Ok(response);
        }

        public static async Task<IResult> CreateAsync(CreateUserPresenter model, IUnitOfWork uow)
        {
            CrudUserFlow workflow = new CrudUserFlow(uow);
            UseCaseResponse response = await workflow.Create(new UserSchema());
            return Results.Ok(response);
        }
    }
}
