﻿using Base.Core;
using Base.Core.Models.Presenters.User;
using Base.Core.Schemas;
using Base.Services; 
using Base.UseCases.User.Crud;

namespace Base.Application.Controllers
{
    public class UserCtrl
    {
        public static async Task<IResult> GetAsync(IUnitOfWork uow)
        {
            CrudUserFlow workflow = new CrudUserFlow(uow);
            UseCaseResponse response = await workflow.List();
            return Results.Ok(response);
        } 

        public static async Task<IResult> CreateAsync(CreateUserPresenter model, IUnitOfWork uow)
        { 
            CrudUserFlow workflow = new CrudUserFlow(uow); 
            UseCaseResponse response = await workflow.Create(new UserSchema());
            return Results.Ok(response);
        }
    }
}
