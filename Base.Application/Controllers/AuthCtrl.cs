﻿
using Base.Core;
using Base.Services;
using Base.UseCases.Auth; 

namespace Base.Application.Controllers
{
    public class AuthCtrl
    { 
        public static IResult Login(string username, string password, IUnitOfWork uow)
        {
            AuthFlow workflow = new AuthFlow(uow);
            UseCaseResponse response = workflow.Login(username, password);
            return Results.Ok(response);
        }
         
        public static IResult RefreshToken(string accessToken, string refreshToken, IUnitOfWork uow)
        {
            AuthFlow workflow = new AuthFlow(uow); 
            UseCaseResponse response = workflow.RefreshToken(accessToken, refreshToken);
            return Results.Ok(response);
        } 
    }
}
