﻿using Base.Core;
using Base.UseCases.Seed;
using Base.Services;
using Base.Application.Routers;

namespace Base.Application.Controllers
{
    public class SeedCtrl
    {  
        public static async Task<IResult> SyncAllPerm(IUnitOfWork uow)
        {
            SeedFlow flow = new SeedFlow(uow);
            var routers = ZRouter.Get();
            UseCaseResponse response = await flow.Seed(routers);
            return Results.Ok(response);
        }
    }
}
