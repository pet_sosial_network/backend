﻿using Base.Core;
using Base.Core.Schemas;

namespace Base.Services
{
    public interface IUser : IBaseService<UserSchema>
    {
        UserSchema SetRefreshToken(string refreshToken, int userId);
        UserSchema UpdateLoginTime(int userId);
        List<UserSchema> FindByName(string name); 
        List<UserSchema> Find(int[] ids); 
    }


    public class UserService : BaseService<UserSchema, DataContext>, IUser
    {

        private readonly DataContext context;

        public UserService(DataContext _ctx) : base(_ctx)
        {
            context = _ctx;
        }

        public UserSchema UpdateLoginTime(int userId)
        {
            UserSchema user = context.Users.Find(userId);
            user.LastLogin = DateTime.UtcNow;
            return user;
        }


        public UserSchema SetRefreshToken(string refreshToken, int userId)
        {
            UserSchema user = context.Users.Find(userId);
            user.HashRefreshToken = refreshToken;
            return user;
        }

        public List<UserSchema> FindByName(string name)
        {
            return context.Users.Where(u => u.UserName.Equals(name)).ToList(); 
        }

        public List<UserSchema> Find(int[] ids)
        {
            return context.Users.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}
