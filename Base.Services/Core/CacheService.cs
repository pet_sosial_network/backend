﻿using Microsoft.Extensions.Caching.Memory;

namespace Base.Services
{
    public interface ICacheService<T> where T : class
    {
        void Set(string cacheKey, List<T> datas);
        List<T> Get(string cacheKey);
    }

    public class CacheService<TEntity> : ICacheService<TEntity>
       where TEntity : class
    {
        private readonly IMemoryCache _memoryCache;
        public CacheService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        public async void Set(string cacheKey, List<TEntity> datas)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromDays(1));
            _memoryCache.Set(cacheKey, datas, cacheEntryOptions);
        }



        public List<TEntity> Get(string cacheKey)
        {
            _memoryCache.TryGetValue(cacheKey, out IEnumerable<TEntity> dataCached);
            return dataCached == null ? null : dataCached.ToList();
        }
    }
}
