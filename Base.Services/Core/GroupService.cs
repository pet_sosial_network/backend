﻿using Base.Core;
using Base.Core.Schemas;

namespace Base.Services
{

    public interface IGroup : IBaseService<GroupSchema>
    {
        List<GroupSchema> Find(int[] ids);
    }

    public class GroupService : BaseService<GroupSchema, DataContext>, IGroup
    {

        private readonly DataContext context;
        public GroupService(DataContext _ctx) : base(_ctx)
        {
            context = _ctx;
        }

        public List<GroupSchema> Find(int[] ids)
        {
            return context.Groups.Where(x => ids.Contains(x.Id)).ToList(); 
        }
    }
}
