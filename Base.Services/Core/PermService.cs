﻿using Base.Core;
using Base.Core.Schemas;

namespace Base.Services
{
    public interface IPerm : IBaseService<Perm>
    {
        List<Perm> GetPerms(int userId);
    }

    public class PermService : BaseService<Perm, DataContext>, IPerm
    {
        private readonly DataContext context;
        public PermService(DataContext _ctx) : base(_ctx)
        {
            context = _ctx;
        }

        public List<Perm> GetPerms(int userId)
        {
            List<Perm> perms = (from p in context.Perms
                                join gp in context.GroupsPerms on p.Id equals gp.PermId
                                join g in context.Groups on gp.GroupId equals g.Id
                                join ug in context.UsersGroups on g.Id equals ug.GroupId
                                where ug.UserId == userId
                                select new Perm { Id = p.Id, Module = p.Module, Action = p.Action, Title = p.Title }).ToList();
            return perms;
        }



    }
}
